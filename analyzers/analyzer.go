package analyzers

import (
	"fmt"
	"go/ast"
	"go/token"
	"regexp"
	"strings"

	"golang.org/x/tools/go/analysis"
)

var RepositoryUsage = &analysis.Analyzer{
	Name: "dirtyusage",
	Doc:  "finds disallowed call/usage of dirty usages",
	Run:  run,
}

var reRepo = regexp.MustCompile("New.*Repository")

func run(pass *analysis.Pass) (interface{}, error) {
	for _, file := range pass.Files {
		ast.Inspect(file, func(n ast.Node) bool {
			needToCheck := true
			if file != nil {
				for _, cc := range file.Comments {
					for _, c := range cc.List {
						if strings.Contains(c.Text, "dirtyusage:allow") {
							needToCheck = false
						}
					}
				}
			}

			if needToCheck {
				if fun, ok := n.(*ast.Ident); ok {
					if f := reRepo.FindString(fun.String()); f != "" {
						pass.Report(analysis.Diagnostic{
							Pos:            fun.Pos(),
							End:            token.Pos(len(fun.String())),
							Category:       "dirtyusage:repositories",
							Message:        fmt.Sprintf("%s calling not from allowed package/file", fun.String()),
							SuggestedFixes: nil,
						})
					}
				}
			}

			return true
		})
	}
	return nil, nil
}
