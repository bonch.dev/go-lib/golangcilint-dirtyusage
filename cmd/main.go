package main

import (
	"golang.org/x/tools/go/analysis"

	"dirtyusage/analyzers"
)

type analyzerPlugin struct{}

func (*analyzerPlugin) GetAnalyzers() []*analysis.Analyzer {
	return []*analysis.Analyzer{
		analyzers.RepositoryUsage,
	}
}

// AnalyzerPlugin used by golangcilint directly
//goland:noinspection GoUnusedGlobalVariable
var AnalyzerPlugin analyzerPlugin
